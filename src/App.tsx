import React, { useRef, useState, useEffect } from "react";
import "./App.css";
import useComponentSize from "@rehooks/component-size";
import cardData from "./Data/data.json";
import uuid from "uuid";
import { Card } from "./Components/Card";
import { Button } from "./Components/Button";
import { Summary } from "./Components/Summary/Summary";
import { AddModal } from "./Components/AddModal";
import { CardInfo } from "./Models/CardInfo";
import { KeyValuePair } from "./Util/KeyValuePair";
import { Offset } from "./Models/Offset";

function positionCards(cards: KeyValuePair<CardInfo>, width: number, height: number) {
    Object.values(cards).forEach(
        (card) =>
            (card.position = {
                left: card.offset.x + width * 0.5,
                top: card.offset.y + height * 0.5,
            }),
    );
}

function parseData(): KeyValuePair<CardInfo> {
    const cards: KeyValuePair<CardInfo> = {};

    cardData.forEach((task) => {
        cards[task.id] = task;
    });

    return cards;
}

function addCard(cards: KeyValuePair<CardInfo>, label: string): void {
    const id = uuid.v4();

    cards[id] = {
        id,
        label,
        offset: {
            x: 0,
            y: 0,
        },
    };
}

const App = () => {
    const [cards, setCards] = useState<KeyValuePair<CardInfo>>({});
    const [dragCardInfo, setDragCardInfo] = useState<{ card: CardInfo; dragOffset: Offset }>(null);
    const [isAddOpen, setIsAddOpen] = useState<boolean>(false);
    const boardRef = useRef<HTMLDivElement>(null);
    const boardSize = useComponentSize(boardRef);
    const { height, width } = boardSize;

    useEffect(() => {
        if (height && width) {
            const parsedCards = parseData();
            positionCards(parsedCards, width, height);
            setCards({ ...parsedCards });
        }
    }, [height, width]);

    const handleDelete = (card: CardInfo) => {
        delete cards[card.id];
        setCards({ ...cards });
    };

    const cardEls = Object.values(cards).map((card) => (
        <Card
            card={card}
            key={card.id}
            onDragStart={(dragOffset) => setDragCardInfo({ card, dragOffset })}
            onDragEnd={() => setDragCardInfo(null)}
            onDoubleClick={() => handleDelete(card)}
        />
    ));

    return (
        <div
            className="App"
            ref={boardRef}
            onMouseMove={(ev) => {
                if (!dragCardInfo) {
                    return;
                }

                const { card, dragOffset } = dragCardInfo;

                card.position = {
                    top: ev.pageY - dragOffset.y,
                    left: ev.pageX - dragOffset.x,
                };

                setCards({ ...cards });
            }}>
            {cardEls}
            <Summary cards={cards} />
            <Button onClick={() => setIsAddOpen(true)}>Add</Button>
            {isAddOpen && (
                <AddModal
                    isOpen={isAddOpen}
                    onClose={() => setIsAddOpen(false)}
                    onAdd={(cardText) => {
                        addCard(cards, cardText);
                        positionCards(cards, width, height);
                        setCards(cards);
                    }}
                />
            )}
        </div>
    );
};

export default App;
