import { Offset } from "./Offset";
import { Position } from "./Position";

export interface CardInfo {
    id: string;
    label: string;
    position?: Position;
    offset?: Offset;
}
