export interface KeyValuePair<V = unknown> {
    [key: string | number]: V;
}
