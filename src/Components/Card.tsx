import { CardInfo } from "../Models/CardInfo";
import { Offset } from "../Models/Offset";

export interface CardProps
    extends Omit<React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>, "onDragStart"> {
    card?: CardInfo;
    onDragStart?: (offset: Offset) => void;
}

export const Card: React.VFC<CardProps> = (props) => {
    const { card, onDragStart, onDragEnd, onDoubleClick, ...rest } = props;
    return (
        <div
            onMouseDown={(ev) => {
                const clickOffset = {
                    x: ev.clientX - parseFloat(ev.currentTarget.style.left),
                    y: ev.clientY - parseFloat(ev.currentTarget.style.top),
                };
                onDragStart(clickOffset);
            }}
            onMouseUp={onDragEnd}
            onDoubleClick={onDoubleClick}
            style={{
                position: "absolute",
                left: card.position.left,
                top: card.position.top,
                backgroundColor: "#fff",
                padding: "25px",
                cursor: "move",
                userSelect: "none",
            }}
            key={card.id}
            {...rest}>
            {card.label}
        </div>
    );
};
