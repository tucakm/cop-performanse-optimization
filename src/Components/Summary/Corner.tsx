import React from "react";
import { PositionType } from "./Summary";

interface CornerProps {
    corner: PositionType;
    position: PositionType;
    setPosition: (position: PositionType) => void;
}

export const Corner: React.VFC<CornerProps> = ({ corner, setPosition, position }) => {
    return (
        <div
            onClick={() => setPosition(corner)}
            className={`arrow arrow-${corner} ${position === corner ? "arrow-active" : ""}`}
        />
    );
};
