import { CardInfo } from "../../Models/CardInfo";
import levenshtein from "levenshtein";
import { KeyValuePair } from "../../Util/KeyValuePair";
import "./Summary.css";
import { useState } from "react";
import { Corner } from "./Corner";

interface SummaryProps {
    cards?: KeyValuePair<CardInfo>;
}

export type PositionType = "top-right" | "top-left" | "bottom-right" | "bottom-left";
export const Summary: React.VFC<SummaryProps> = (props) => {
    const [position, setPosition] = useState<PositionType>("top-right");
    const cards = Object.values(props.cards);

    const distances = { max: 0, min: 100000 };
    cards.forEach((currentCard) => {
        cards.forEach((compareCard) => {
            if (compareCard === currentCard) {
                return;
            }
            const distance = new levenshtein(currentCard.label, compareCard.label);

            distances.max = Math.max(distances.max, distance.distance);
            distances.min = Math.min(distances.min, distance.distance);
        });
    });

    return (
        <div className={`Summary Summary-${position}`}>
            <div>You have {Object.keys(cards).length} cards!</div>
            <div>Max difference in labels: {distances.max}</div>
            <div>Min difference in labels: {distances.min}</div>
            <Corner setPosition={setPosition} corner="top-right" position={position} />
            <Corner setPosition={setPosition} corner="top-left" position={position} />
            <Corner setPosition={setPosition} corner="bottom-left" position={position} />
            <Corner setPosition={setPosition} corner="bottom-right" position={position} />
        </div>
    );
};
